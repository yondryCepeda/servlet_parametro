package com.yondry.main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.eclipse.persistence.jpa.rs.util.metadatasources.LinkV2MetadataSource;

import com.yondry.entidad.Libro;

public class Many_to_many implements Comparator<Libro>{
	
	public List<Libro> metodoUno(List <Libro> lista){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("hola");
		EntityManager em = emf.createEntityManager();
		
		List <Libro> libroLista = em.createNamedQuery("Libro.buscarLibros").getResultList();
		List <Libro> listaNueva = new ArrayList<Libro>();
		for(int i=0;i<lista.size(); i++){
			if(libroLista.contains(lista.get(i)) == false){
				listaNueva.add(lista.get(i));
			}
		}
		return listaNueva;
	}
	
	public List<Libro> metodoDos(List <String> lista){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("hola");
		EntityManager em = emf.createEntityManager();
		
		List <Libro> libroLista = em.createNamedQuery("Libro.buscarLibros").getResultList();
		List <Libro> listaNueva = new ArrayList();
		
		Libro li = new Libro();
		
		
		for(String list: lista){
		
			if(libroLista.contains(list) == false){
				listaNueva = (List) lista;
			}
		}
		if(libroLista.contains(listaNueva)){
			return libroLista;	
		}
		Collections.sort(libroLista,new Many_to_many());
		return libroLista;
	}
	
	public static void main(String[] args){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("hola");
		EntityManager em = emf.createEntityManager();
		
		List <Libro> lista =new ArrayList();
	
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);;
		java.util.Date result =cal.getTime();
		
		Libro l1 = new Libro();
		l1.setNombre("Matematicas");
		l1.setFecha(result);
		
		cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -2);
		java.util.Date result2 =cal.getTime();
		
		Libro l2 = new Libro();
		l2.setNombre("Naturales");
		l2.setFecha(result2);
		
		cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -3);
		java.util.Date result3 =cal.getTime();
		
		Libro l3 = new Libro();
		l3.setNombre("Sociales");
		l3.setFecha(result3);
		
		cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -4);
		java.util.Date result4 =cal.getTime();
		
		Libro l4 = new Libro();
		l4.setNombre("ingles");
		l4.setFecha(result3);
		
		lista.add(l1);
		lista.add(l2);
		lista.add(l3);
		lista.add(l4);
		
		List<String> listaDos = new ArrayList<String>();
		listaDos.add("Matematicas");
		listaDos.add("Ingles");
		listaDos.add("Frances");
		
	
		Many_to_many mm = new Many_to_many();
		System.out.println("Metodo 1: "+mm.metodoUno(lista));
			
		Many_to_many m = new Many_to_many();
		System.out.println("Metodo 2: "+m.metodoDos(listaDos));
	}

	public int compare(Libro o1, Libro o2) {
		Libro li1 = (Libro) o1;
		Libro li2 = (Libro) o2;
		
		return li2.getFecha().compareTo(li1.getFecha());
	}

}

