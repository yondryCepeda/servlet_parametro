
<%@page import="entidad.Animal" %>
<%@page import="java.util.List" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">	
<title>Animales</title>
</head>
<body>

<%
List<Animal> lista = (List<Animal>) request.getAttribute("lista");
%>
<ul>
<%
for(Animal animal: lista) {
%>
	<li><% out.print(animal.getNombre()+" "+animal.getEdad()); %></li>
<%}%>
</ul>


</body>
</html>