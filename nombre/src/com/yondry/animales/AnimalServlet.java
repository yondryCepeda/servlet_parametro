package com.yondry.animales;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import entidad.Animal; ;
public class AnimalServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Animal> lista =new ArrayList<Animal>();
		lista.add(new Animal("Perro" , 10));
		lista.add(new Animal("Gato" , 3));
		lista.add(new Animal("Gallina" , 2));
		lista.add(new Animal("Gusano" , 1));
		lista.add(new Animal("Mariposa" , 1));
		req.setAttribute("lista", lista);
		req.getRequestDispatcher("/animal-lista.jsp").forward(req, resp);
	}
	
	
	

}
